package us.b3k.kafka.ws.consumer;

import kafka.consumer.ConsumerConfig;
import us.b3k.kafka.ws.transforms.Transform;

import javax.websocket.Session;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KafkaConsumerFactory {
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    private final Properties configProps;
    private final Transform outputTransform;

    static public KafkaConsumerFactory create(Properties configProps, Class outputTransformClass) throws IllegalAccessException, InstantiationException {
        Transform outputTransform = (Transform)outputTransformClass.newInstance();
        outputTransform.initialize();
        return new KafkaConsumerFactory(configProps, outputTransform);
    }

    private KafkaConsumerFactory(Properties configProps, Transform outputTransform) {
        this.configProps = configProps;
        this.outputTransform = outputTransform;
    }

    public KafkaConsumer getConsumer(final String topics, final Session session) {
        return getConsumer(Arrays.asList(topics.split(",")), session);
    }

    public KafkaConsumer getConsumer(final List<String> topics, final Session session) {
        String groupId = "";
        //if (configProps.containsKey("group.id")) {
        //    groupId = String.format("%s-old", configProps.getProperty("group.id"));
        //}

        if (groupId.isEmpty()) {
            groupId = String.format("%s-%d", session.getId(), System.currentTimeMillis());
        }

        Properties sessionProps = (Properties)configProps.clone();
        sessionProps.setProperty("group.id", groupId);
        //sessionProps.setProperty("key", key);

        KafkaConsumer consumer = new KafkaConsumer(new ConsumerConfig(sessionProps), executorService, outputTransform, topics, session);
        consumer.start();
        return consumer;
    }
}
