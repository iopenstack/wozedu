#!/usr/bin/env ruby
require 'faye/websocket'
require 'eventmachine'
require 'json'
 
EM.run {
  ws = Faye::WebSocket::Client.new('ws://localhost:7080/v1/?topic=wozedu_topic1', ['kafka-text'])
#  ws = Faye::WebSocket::Client.new('ws://localhost:7080/v1/?topics=my_topic&group.id=kafka-websocket-client', ['kafka-text'])
 
  ws.on :open do |event|
    p [:open]
    msg = {:topic => "wozedu_topic4", :key => "id", :message => "my awesome message"}
    for i in 0..9
       puts "Value of local variable is #{i}"
       ws.send(msg.to_json)
    end
  end
 
  ws.on :message do |event|
    p [:message, event.data]
    p JSON.parse(event.data)
  end
 
  ws.on :close do |event|
    p [:close, event.code, event.reason]
    ws = nil
  end
}

